export default function authHeader() {
	const user = JSON.parse(localStorage.getItem('user'));
	const token = user.result;
	if (token) {
		return { Authorization: token };
	} else {
		return {};
	}
}
