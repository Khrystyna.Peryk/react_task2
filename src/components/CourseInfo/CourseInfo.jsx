import React from 'react';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

const CourseInfo = () => {
	const location = useLocation();
	return (
		<div>
			<div>
				<Link to='/courses'>&#60; Back to courses</Link>
			</div>
			<section>
				<h2>{location.state.title}</h2>
				<div>
					<div>{location.state.description}</div>
					<div>
						<p>
							<b>ID: </b>
							{location.state.id}
						</p>
						<p>
							<b>Duration: </b>
							{location.state.duration} hours
						</p>
						<p>
							<b>Created: </b>
							{location.state.creationDate}
						</p>
						<p>
							<b>Authors: </b>
							{location.state.authors}
						</p>
					</div>
				</div>
			</section>
		</div>
	);
};

export default CourseInfo;
